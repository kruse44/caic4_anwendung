# Dataprep for application of cAIC Model Averaging 

# loading libraries
library(dplyr)

# loading data
data_original <- read.csv("data/Datensatz_7_10.csv")
data <- data_original

# Problematic data entries:
# - negativ education in year (bilzeit)
data <- data[data$bilzeit>0, ]
# - isced negativ not possible
data <- data[data$isced>=0, ]
# - negativ experience not possible
data <- data[data$expft>=0, ]
data <- data[data$exppt>=0, ]
# - negativ labour income 
data <- data[data$labylM>0, ]

# Calculating metrics
# log of monthly labour income
data$log_labylm <- log(data$labylM)
# removing NAs
data <- na.omit(data)
# binary dummy for higher education (isced 5 + 6)
data$educ <- NA
data$educ[data$isced %in% c(5,6)] <- 1
data$educ[data$isced %in% c(1,2,3,4)] <- 0

# Subsetting the data
# creating a subset only foe east-germany
# Bula dummies after SOEP classifications 
# Source: https://www.diw.de/documents/publikationen/73/diw_01.c.55738.de/diw_datadoc_2007-017.pdf
# East:
  # 11 - Berlin
  # 12 - Meck-Pom
  # 13 - Brandenburg
  # 14 - Sachsen-Anhalt
  # 15 - Thüringen
  # 16 - Sachsen
eaststates <- 11:16 
# east germany dataset
data_east <- filter(data, bula %in% eaststates)
# Only for the year 2007
data2007 <- subset(data,year==2007)

# saving the datasets
saveRDS(data, file = "data/data.RDS")
saveRDS(data_east, file = "data/data_east.RDS")
saveRDS(data2007, file = "data/data2007.RDS")
