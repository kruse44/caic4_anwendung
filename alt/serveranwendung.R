# R Code für die Anwendung im Paper 
# Version: 0.1.0
# datum: 31.1.2020

# Aufgabe:
# Daten von Alexander vom SOEP mit verschiedenen modelle fitten und dann über model averaging arbeiten
# Hier wurde Multi-Core Anwendungen des cAIC4 programmiert (zufinden auf Github-Repo "helpeR").

# Libraries laden:
library(helpeR)
library(splines)
library(parallel)

# Environment:
set.seed(42)
discrib      <- "Berechnung von des cAIC für die Anwendung im Paper: model AVeraging via Aug Lag"
filedate     <- paste0("/data/", "results_",gsub(" ", "_", 
                                                 gsub(":", "-", Sys.time())), ".Rdata")
pfad         <- getwd()
sessInfo     <- sessionInfo()
systemInfo   <- Sys.info()
threads      <- detectCores() 


data <- readRDS("data/data2007_10.RDS")
kandidaten <- list(m11 <- lmer(log_labylm ~  age + bilzeit + I(bilzeit^2) + (1 | health),
                                data = data, REML = TRUE),
                    m21 <- lmer(log_labylm ~  (age) + bs(bilzeit) + (1 | health),
                                data = data, REML = TRUE),
                    m00 <- lm(log_labylm ~  age + bilzeit + I(bilzeit^2), 
                              data = data)
)

data1 <- readRDS("data/data2007_20.RDS")
kandidaten1 <- list( m10 <- lmer(log_labylm ~  age + bilzeit + I(bilzeit^2) + (1 | bula),
                                 data = data1, REML = TRUE),
                     m11 <- lmer(log_labylm ~  age + bilzeit + I(bilzeit^2) + (1 | health),
                                 data = data1, REML = TRUE),
                     m12 <- lmer(log_labylm ~  age + bilzeit + I(bilzeit^2) + (1 | marstat),
                                 data = data1, REML = TRUE),
                     m20 <- lmer(log_labylm ~  (age) + bs(bilzeit) + (1 | bula),
                                 data = data1, REML = TRUE),
                     m21 <- lmer(log_labylm ~  (age) + bs(bilzeit) + (1 | health),
                                 data = data1, REML = TRUE),
                     m23 <- lmer(log_labylm ~  (age) + bs(bilzeit) + (1 | marstat),
                                 data = data1, REML = TRUE),
                     m00 <- lm(log_labylm ~  age + bilzeit + I(bilzeit^2), 
                               data = data1)
)
# Start
starttime   <- Sys.time()
anoc0  <- helpeR::MC_anocAIC(kandidaten)
duration_anoc0 <- Sys.time() - starttime

starttime   <- Sys.time()
anoc1  <- helpeR::MC_anocAIC(kandidaten1)
duration_anoc1 <- Sys.time() - starttime

output <- list(
  "System Information" = systemInfo,
  "Session Infomation" = sessInfo,
  "Anzahl Threads"  = threads,
  "Results10" = anoc0,
  "Duration10" = duration_anoc0,
  "Results20" = anoc1,
  "Duration20" = duration_anoc1
)

saveRDS(output, file = paste0((pfad), filedate))
  