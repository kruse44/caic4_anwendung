# Simulationen fuer das Paper
set.seed(1)
# Libries
library(cAIC4)
library(foreach)
library(doParallel)

# Simulation Nr 1
# Beobachtungen (y) sibeta0lieren, wobei die folgende Gleichungen gelten:
  # Fall 1:
    # y = 2 * x + 2 * z 
  # Fall 2:
    # y = 2 * x + 3 * z 
# Und jeweils für den Fall, dass der fixe Intercept nicht 0 ist und unterschiedlich


# hierbei sein x und z wie folgt definiert:
  # x(z) = 1 + (1 | b)

# Es werden hierbei keine Nebenbedingungen für die Gewichte unterstellt.
# Bei den Berechneten Gewichten sollten im unbeschränkten Fall eine Gewichtung von 2 : 2 bzw. 2 : 3 herauskommen.
# Unter der Annahme von Informationserhaltung sollte hierbei Gewichte von 1/2 : 1/2 und 2/5 : 3/5 herauskommen.

# Funktion zur Simulation der Daten:
sim_fun = function(ngroup = 10, nind = 10, factor_x, factor_z, beta0_x = 0, beta0_z = 0, 
                   sigma_group_x, sigma_group_z, sigma_res = 1) {
  # für x:
  groupeff_x = rep( rnorm(ngroup, 0, sigma_group_x), each = nind)
  group_x = rep(LETTERS[1:ngroup], each = nind)
  res_x = rnorm(ngroup*nind, 0, sigma_res)
  resp_x = beta0_x + groupeff_x + res_x
  # für z: 
  groupeff_z = rep( rnorm(ngroup, 0, sigma_group_z), each = nind)
  group_z = rep(LETTERS[1:ngroup], each = nind)
  res_z = rnorm(ngroup*nind, 0, sigma_res)
  resp_z = beta0_z + groupeff_z + res_z
  # Ergebnis:
  resp <- resp_x * factor_x + resp_z * factor_z
  dat = data.frame(resp, "group" = group_z, resp_x, resp_z,beta0_x, beta0_z, groupeff_x, groupeff_z, res_x, res_z)
  return(dat)
}


#### FALL 1 (y=2*x+2*z)

# Simulation von y im Fall 1 (y_1).
  # Struktur der Daten:
    # mit Gruppen = 10, Individuen pro Gruppe = 10, 
  # Faktor des Einflusses:
    # factor_x = 2, factor_z = 2
  # Fixe Effekte:
    # beta0_x = 0, beta0_z = 0,
  # Sd Random Effekt: 
    # sigma_group_x = 2, sigma_group_z =3 
    # Sd Residuen (sigma_res) = 1
y_1 <- sim_fun(factor_x = 2, factor_z = 2, beta0_x = 0, beta0_z = 0, sigma_group_x = 2, sigma_group_z = 3)

# Simulation von y im Kontroll-Fall 1 (y_1k1).
# Unterschiedliche Fixe Effekte
  # Struktur der Daten:
    # mit Gruppen = 10, Individuen pro Gruppe = 10, 
  # Faktor des Einflusses:
    # factor_x = 2, factor_z = 2
  # Fixe Effekte:
    # beta0_x = 1, beta0_z = 3,
  # Sd Random Effekt: 
    # sigma_group_x = 2, sigma_group_z =3 
    # Sd Residuen (sigma_res) = 1
y_1k1 <- sim_fun(factor_x = 2, factor_z = 2, beta0_x = 1, beta0_z = 3, sigma_group_x = 2, sigma_group_z = 3)

# Simulation von y im Kontroll-Fall 1 (y_1k2).
# Viel Größere Stichprobe
  # Struktur der Daten:
    # mit Gruppen = 100, Individuen pro Gruppe = 100, 
  # Faktor des Einflusses:
    # factor_x = 2, factor_z = 2
  # Fixe Effekte:
    # beta0_x = 0, beta0_z = 0,
  # Sd Random Effekt: 
    # sigma_group_x = 2, sigma_group_z =3 
    # Sd Residuen (sigma_res) = 1
y_1k2 <- sim_fun(ngroup = 100, nind = 100, factor_x = 2, factor_z = 2, beta0_x = 0, beta0_z = 0, sigma_group_x = 2, sigma_group_z = 3)

# Simulation von y im Kontroll-Fall 1 (y_1k3).
# Größere Unterschiede zwischen Random Effekt Varianzen
  # Struktur der Daten:
    # mit Gruppen = 10, Individuen pro Gruppe = 10, 
  # Faktor des Einflusses:
    # factor_x = 2, factor_z = 2
  # Fixe Effekte:
    # beta0_x = 0, beta0_z = 0,
  # Sd Random Effekt: 
    # sigma_group_x = 1, sigma_group_z = 6 
    # Sd Residuen (sigma_res) = 1
y_1k3 <- sim_fun(factor_x = 2, factor_z = 2, beta0_x = 0, beta0_z = 0, sigma_group_x = 1, sigma_group_z = 6)

#### FALL 2 (y=2*x+3*z)

# Simulation von y im Fall 2 (y_2).
  # Struktur der Daten:
    # mit Gruppen = 10, Individuen pro Gruppe = 10, 
  # Faktor des Einflusses:
    # factor_x = 2, factor_z = 3
  # Fixe Effekte:
    # beta0_x = 0, beta0_z = 0,
  # Sd Random Effekt: 
    # sigma_group_x = 2, sigma_group_z = 3 
    # Sd Residuen (sigma_res) = 1
y_2 <- sim_fun(factor_x = 2, factor_z = 3, beta0_x = 0, beta0_z = 0, sigma_group_x = 2, sigma_group_z = 3)

# Simulation von y im Kontroll Fall 1 (y_2k1).
  # Struktur der Daten:
    # mit Gruppen = 10, Individuen pro Gruppe = 10, 
  # Faktor des Einflusses:
    # factor_x = 2, factor_z = 3
  # Fixe Effekte:
    # beta0_x = 1, beta0_z = 10,
  # Sd Random Effekt: 
    # sigma_group_x = 2, sigma_group_z = 3 
    # Sd Residuen (sigma_res) = 1
y_2k1 <- sim_fun(factor_x = 2, factor_z = 3, beta0_x = 0, beta0_z = 0, sigma_group_x = 2, sigma_group_z = 3)

# Simulation von y im Kontroll Fall 1 (y_2k2).
  # Struktur der Daten:
    # mit Gruppen = 10, Individuen pro Gruppe = 10, 
  # Faktor des Einflusses:
    # factor_x = 2, factor_z = 3
  # Fixe Effekte:
    # beta0_x = 0, beta0_z = 0,
  # Sd Random Effekt: 
    # sigma_group_x = 2, sigma_group_z = 3 
    # Sd Residuen (sigma_res) = 1
y_2k2 <- sim_fun(factor_x = 2, factor_z = 3, beta0_x = 0, beta0_z = 0, sigma_group_x = 2, sigma_group_z = 3)
